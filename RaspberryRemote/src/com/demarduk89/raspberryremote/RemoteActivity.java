package com.demarduk89.raspberryremote;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class RemoteActivity extends Activity
{
    String ip = "";
    int port = 0;
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        
        SharedPreferences sharedPrefIp = this.getSharedPreferences(getString(R.string.pref_file_ip), Context.MODE_PRIVATE);
        String defaultValueIp = getResources().getString(R.string.pref_file_ip_default);    
        String actualIp = sharedPrefIp.getString(getString(R.string.pref_file_ip), defaultValueIp);
        
        SharedPreferences sharedPrefPort = this.getSharedPreferences(getString(R.string.pref_file_port), Context.MODE_PRIVATE);
        int defaultValuePort = Integer.parseInt(getResources().getString(R.string.pref_file_port_default));    
        int actualPort = sharedPrefIp.getInt(getString(R.string.pref_file_port), defaultValuePort);
        
        ip = actualIp;
        port = actualPort;
        TextView tv = (TextView)findViewById(R.id.actualIp);
        tv.setText(tv.getText()+"  "+actualIp+":"+Integer.toString(actualPort));
    }
    
    public void startSMB(View v) throws IOException{
        String sentence = "startsmb\n";
        sendMsg(sentence);        
    }
    
    public void stopSMB(View v) throws IOException{
        String sentence = "stopsmb\n";
        sendMsg(sentence);
    }
    
    public void startFTP(View v) throws IOException{
        String sentence = "startftp\n";
        sendMsg(sentence);        
    }
    
    public void stopFTP(View v) throws IOException{
        String sentence = "stopftp\n";
        sendMsg(sentence);
    }
    
    public void startDLNA(View v) throws IOException{
        String sentence = "startminidlna\n";
        sendMsg(sentence);
    }
    
    public void stopDLNA(View v) throws IOException{
        String sentence = "stopminidlna\n";
        sendMsg(sentence);
    }
    
    public void mountHD(View v) throws IOException{
        String sentence = "mountpi\n";
        sendMsg(sentence);
    }
    
    public void umountHD(View v) throws IOException{
        String sentence = "umountpi\n";
        sendMsg(sentence);
    }
    
    public void cleanDLNA(View v) throws IOException{
        String sentence = "minidlnacln\n";
        sendMsg(sentence);
    }
    
    public void halt(View v) throws IOException{
        String sentence = "halt\n";
        sendMsg(sentence);
    }
    
    public void reboot(View v) throws IOException{
        String sentence = "reboot\n";
        sendMsg(sentence);
    }
    
    public void setIp(View v){
        EditText etip = (EditText)findViewById(R.id.address);
        EditText etport = (EditText)findViewById(R.id.port);
        
        if(!(etip.getText().toString()).equals("")){
            SharedPreferences sharedPrefIp = this.getSharedPreferences(getString(R.string.pref_file_ip), Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPrefIp.edit();
            editor.putString(getString(R.string.pref_file_ip), etip.getText().toString());
            editor.commit();
        }
        if(!(etport.getText().toString()).equals("")){
            SharedPreferences sharedPrefPort = this.getSharedPreferences(getString(R.string.pref_file_port), Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPrefPort.edit();
            editor.putInt(getString(R.string.pref_file_port), Integer.parseInt(etport.getText().toString()));
            editor.commit();
        }
        
        SharedPreferences sharedPrefIp = this.getSharedPreferences(getString(R.string.pref_file_ip), Context.MODE_PRIVATE);
        String defaultValueIp = getResources().getString(R.string.pref_file_ip_default);    
        String actualIp = sharedPrefIp.getString(getString(R.string.pref_file_ip), defaultValueIp);
        
        SharedPreferences sharedPrefPort = this.getSharedPreferences(getString(R.string.pref_file_port), Context.MODE_PRIVATE);
        int defaultValuePort = Integer.parseInt(getResources().getString(R.string.pref_file_port_default));    
        int actualPort = sharedPrefIp.getInt(getString(R.string.pref_file_port), defaultValuePort);
        
        TextView tv = (TextView)findViewById(R.id.actualIp);
        tv.setText(getString(R.string.actip)+"  "+actualIp+":"+Integer.toString(actualPort));
        
        ip = actualIp;
        port = actualPort;   
    }
    
     private void sendMsg(String sentence) throws IOException{
        String response;
        Socket clientSocket = new Socket(ip, port);
        DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
        BufferedReader inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        outToServer.writeBytes(sentence);
        response = inFromServer.readLine();
        if(response.equals("Ok")){
            Context context = getApplicationContext();
            CharSequence text = getResources().getString(R.string.ok);
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
        }
        else {
            Context context = getApplicationContext();
            CharSequence text = getResources().getString(R.string.error);
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
        }
        clientSocket.close();
        boolean closed = clientSocket.isClosed();
        if(closed) Log.d("Socket", "closed");
        else Log.d("Socket", "open");
    }
    
    
}
