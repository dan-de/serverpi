#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <errno.h>
#include "./inih/ini.h"

#define BACKLOG       10
#define MAXLINE     1024

typedef struct{
	int port;
	char* mountcmd;
	char* umountcmd;
	char* dlnadb;
} config;
void configinit(config* cfg){
	cfg->port=0;
	cfg->mountcmd=malloc(128*sizeof(char));
	cfg->umountcmd=malloc(128*sizeof(char));
	cfg->dlnadb=malloc(128*sizeof(char));
}

config* cfg;
char* conf_ini_path = "/opt/serverPi/config.ini";

//Lettura di messaggio su socket. Simbolo di fine riga "\n"
ssize_t ReadLine(int fd, void *vptr, int maxlen)
{
	int n, rc;
	char c, *ptr;

	ptr = vptr;
	for (n = 1; n < maxlen; n++) {
		if ((rc = read(fd, &c, 1)) == 1) {
			*ptr++ = c;
			if (c == '\n')
				n = maxlen;
		} else if (rc == 0) {	/* read ha letto l'EOF */
			if (n == 1)
				return (0);	/* esce senza aver letto nulla */
			else
				break;
		} else
			return (-1);	/* errore */
	}
	ptr--;
	*ptr = 0;
	return (n);		/* restituisce il numero di byte letti */
}

ssize_t WriteLine(int fd, const void *buf, size_t count)
{
	size_t nleft;
	ssize_t nwritten;
	char ch = '\n';

	nleft = count;
	while (nleft > 0) {
		if ((nwritten = write(fd, buf, nleft)) < 0) {
			if (errno == EINTR) {
				continue;
			} else {
				return (nwritten);
			}
		}
		nleft -= nwritten;
		buf += nwritten;

	}

	if ((nwritten = write(fd, &ch, 1)) < 0) {
		fprintf(stderr, "Errore scrittura carattere chiusura WriteLine\n");
		//exit(EXIT_FAILURE);
	}

	return (nleft);
}

void actions(int fd, char buff[])
{
	int return_code;
	char *response = malloc(sizeof(char) * MAXLINE);

	printf("Received: %s --> ", buff);
	fflush(stdout);
	if (strcmp(buff, "mountpi") == 0) {
		return_code = system(cfg->mountcmd);
		if (return_code < 0) {
			printf("Error in mountpi\n");
			fflush(stdout);
			strcpy(response, "Error");
			WriteLine(fd, response, strlen(response));
		} else {
			printf("Ok\n");
			fflush(stdout);
			strcpy(response, "Ok");
			WriteLine(fd, response, strlen(response));
		}
	} else if (strcmp(buff, "umountpi") == 0) {
		return_code = system(cfg->umountcmd);
		if (return_code < 0) {
			printf("Error in umountpi\n");
			fflush(stdout);
			strcpy(response, "Error");
			WriteLine(fd, response, strlen(response));
		} else {
			printf("Ok\n");
			fflush(stdout);
			strcpy(response, "Ok");
			WriteLine(fd, response, strlen(response));
		}
	} else if (strcmp(buff, "startsmb") == 0) {
		return_code = system("service samba start");
		if (return_code < 0) {
			printf("Error in startsmb\n");
			fflush(stdout);
			strcpy(response, "Error");
			WriteLine(fd, response, strlen(response));
		} else {
			printf("Ok\n");
			fflush(stdout);
			strcpy(response, "Ok");
			WriteLine(fd, response, strlen(response));
		}
	} else if (strcmp(buff, "stopsmb") == 0) {
		return_code = system("service samba stop");
		if (return_code < 0) {
			printf("Error in stopsmb\n");
			fflush(stdout);
			strcpy(response, "Error");
			WriteLine(fd, response, strlen(response));
		} else {
			printf("Ok\n");
			fflush(stdout);
			strcpy(response, "Ok");
			WriteLine(fd, response, strlen(response));
		}
	} else if (strcmp(buff, "startftp") == 0) {
		return_code = system("service proftpd start");
		if (return_code < 0) {
			printf("Error in startftp\n");
			fflush(stdout);
			strcpy(response, "Error");
			WriteLine(fd, response, strlen(response));
		} else {
			printf("Ok\n");
			fflush(stdout);
			strcpy(response, "Ok");
			WriteLine(fd, response, strlen(response));
		}
	} else if (strcmp(buff, "stopftp") == 0) {
		return_code = system("service proftpd stop");
		if (return_code < 0) {
			printf("Error in stopftp\n");
			fflush(stdout);
			strcpy(response, "Error");
			WriteLine(fd, response, strlen(response));
		} else {
			printf("Ok\n");
			fflush(stdout);
			strcpy(response, "Ok");
			WriteLine(fd, response, strlen(response));
		}
	} else if (strcmp(buff, "startminidlna") == 0) {
		return_code = system("service minidlna start");
		if (return_code < 0) {
			printf("Error in startminidlna\n");
			fflush(stdout);
			strcpy(response, "Error");
			WriteLine(fd, response, strlen(response));
		} else {
			printf("Ok\n");
			fflush(stdout);
			strcpy(response, "Ok");
			WriteLine(fd, response, strlen(response));
		}
	} else if (strcmp(buff, "stopminidlna") == 0) {
		return_code = system("service minidlna stop");
		if (return_code < 0) {
			printf("Error in stopminidlna\n");
			fflush(stdout);
			strcpy(response, "Error");
			WriteLine(fd, response, strlen(response));
		} else {
			printf("Ok\n");
			fflush(stdout);
			strcpy(response, "Ok");
			WriteLine(fd, response, strlen(response));
		}
	} else if (strcmp(buff, "minidlnacln") == 0) {
		return_code = system("service minidlna stop");
		if (return_code < 0) {
			printf("Error in stopminidlna\n");
			fflush(stdout);
			strcpy(response, "Error");
			WriteLine(fd, response, strlen(response));
			return;
		}
		return_code = system(cfg->dlnadb);
		if (return_code < 0) {
			printf("Error in minidlnacln\n");
			fflush(stdout);
			strcpy(response, "Error");
			WriteLine(fd, response, strlen(response));
			return;
		}
		return_code = system("service minidlna start");
		if (return_code < 0) {
			printf("Error in startminidlna\n");
			fflush(stdout);
			strcpy(response, "Error");
			WriteLine(fd, response, strlen(response));
		} else {
			printf("Ok\n");
			fflush(stdout);
			strcpy(response, "Ok");
			WriteLine(fd, response, strlen(response));
		}
	} else if (strcmp(buff, "halt") == 0) {
		printf("Ok\n");
		fflush(stdout);
		strcpy(response, "Ok");
		WriteLine(fd, response, strlen(response));
		return_code = system("halt");
	} else if (strcmp(buff, "reboot") == 0) {
		printf("Ok\n");
		fflush(stdout);
		strcpy(response, "Ok");
		WriteLine(fd, response, strlen(response));
		return_code = system("reboot");
	}
	free(response);

}

static int handler(void* initialization, const char* section, const char* name,
                   const char* value)
{
    config* pconfig = (config*)initialization;

    #define MATCH(s, n) strcmp(section, s) == 0 && strcmp(name, n) == 0
    if (MATCH("protocol", "port")) {
        pconfig->port = atoi(value);
    } else if (MATCH("command", "mountcmd")) {
        pconfig->mountcmd = strdup(value);
    } else if (MATCH("command", "umountcmd")) {
        pconfig->umountcmd = strdup(value);
    } else if (MATCH("command", "dlnadb")) {
        pconfig->dlnadb = strdup(value);
    } else {
        return 0;  /* unknown section/name, error */
    }
    return 1;
}

int main()
{	
	int listensd, connsd;
	struct sockaddr_in servaddr;
	char buff[MAXLINE];
	int pid, optval;
	
	cfg=malloc(sizeof(config));
	configinit(cfg);
	
	if (ini_parse(conf_ini_path, handler, cfg) < 0) {
        	printf("Can't load 'config.ini'\n");
        	return 1;
    }
    printf("Config loaded from 'config.ini': port=%d, mountcmd=%s, umountcmd=%s, dlnadb=%s\n",cfg->port, cfg->mountcmd, cfg->umountcmd, cfg->dlnadb);
    fflush(stdout);

	if ((listensd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {	/* crea il socket */
		perror("errore in socket");
		exit(1);
	}

	memset((void *) &servaddr, 0, sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = htonl(INADDR_ANY);	/* il server accetta 
							   connessioni su una qualunque delle sue intefacce di rete */
	servaddr.sin_port = htons(cfg->port);	/* numero di porta del server */

	optval = 1;
	if (setsockopt(listensd, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(int)) < 0) {
		fprintf(stderr, "Error in setsockopt: %d : %s\n", errno, strerror(errno));
		exit(EXIT_FAILURE);
	}

	/* assegna l'indirizzo al socket */
	if ((bind(listensd, (struct sockaddr *) &servaddr, sizeof(servaddr))) < 0) {
		perror("errore in bind");
		exit(1);
	}

	if (listen(listensd, BACKLOG) < 0) {
		perror("errore in listen");
		exit(1);
	}

	for (;;) {
		if ((connsd = accept(listensd, (struct sockaddr *) NULL, NULL)) < 0) {
			perror("errore in accept");
			exit(1);
		}
		//printf("Connessione accettata\n");

		pid = fork();
		if (pid < 0) {
			printf("Cannot fork!!\n");
			exit(1);
		}

		if (pid == 0) {
			close(listensd);
			ReadLine(connsd, &buff, MAXLINE);
			printf("Letto: %s\n", buff);
			fflush(stdout);
			actions(connsd, buff);
			if (close(connsd) == -1) {	/* chiude la connessione */
				perror("errore in close");
				exit(1);
			}
			free(cfg->mountcmd);
			free(cfg->umountcmd);
			free(cfg->dlnadb);
			free(cfg);
			
			exit(0);
		}
		else{
			waitpid(pid, NULL, WUNTRACED);
		}

		if (close(connsd) == -1) {	/* chiude la connessione */
			perror("errore in close");
			exit(1);
		}
	}
	exit(0);
}
